package common

import "math/big"

/*
	This source code file, 'combinationspermutations.go', is located in source code repository:

  https://github.com/MikeAustin71/mathhlpr.git

	NOTE: This source code file is still in development.

*/

// Reference: Combinations
// https://www.youtube.com/watch?v=wJDKqEYq7SY
// Combinations - Where Order does not matter
// Combinations Formula nCr
// n = n things
// r = n things taken r at a time
// r is never bigger than n
//    n!
// -------
// r! (n-r)!
// always resolve (n-r)! first

// Reference: Permutations
// https://www.youtube.com/watch?v=IGNO5ucy6eY
// Permutations Formula = nPr
//  nPr =
//         n!
//    --------------
//       (n - r)!
//
// n = total number of elements is the set
// r = number of elements in each permutation
//
//

type CombinationsOp struct {
	nThings          *big.Int
	rAtATime         *big.Int
	NoOfCombintaions *big.Int
}

/*
func (comOp *CombinationsOp) GetCombinations(n *big.Int, r *big.Int) (*big.Int, error) {
	var nFac []*big.Int
	newFac := big.NewInt(0).Set(n)

	for

	return big.NewInt(0), nil
}
*/
