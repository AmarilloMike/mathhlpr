package common

import (
	"math"
	"math/big"
)

/*
	This source code file, 'primefactors.go', is located in source code repository:

  https://github.com/MikeAustin71/mathhlpr.git

*/

type PrimeFactorOp struct {
	BaseNum      uint64
	FacsAndExpts [][][]uint64 // Prime Factors and Exponents

}

func (pf *PrimeFactorOp) PrimeFactorization(number uint64) error {

	pf.FacsAndExpts = make([][][]uint64, 0)

	n := number
	limit := pf.SquareRootUint64(n)

	for i := uint64(2); i <= limit; i++ {

		if pf.ModUint64(n, i) == 0 {
			ct := uint64(0)
			for pf.ModUint64(n, i) == 0 {
				n = n / i
				ct++
			}

			limit = pf.SquareRootUint64(n)

			pf.FacsAndExpts = append(pf.FacsAndExpts, [][]uint64{{i, ct}})
		}

	}

	if n != 1 {
		pf.FacsAndExpts = append(pf.FacsAndExpts, [][]uint64{{n, 1}})
	}

	return nil
}

func (pf *PrimeFactorOp) IsNumPrimeUint64(number uint64) bool {

	if number < 11 {
		switch number {
		case 1:
			return false
		case 3:
			return true
		case 4:
			return false
		case 5:
			return true
		case 6:
			return false
		case 7:
			return true
		case 8:
			return false
		case 9:
			return false
		case 10:
			return false
		}
	}

	limit := pf.SquareRootUint64(number)
	for i := uint64(2); i <= limit; i++ {

		if pf.ModUint64(number, i) == 0 {
			return false
		}

	}

	return true
}

func (pf *PrimeFactorOp) ModUint64(dividend, divisor uint64) uint64 {

	return dividend - ((dividend / divisor) * divisor)
}

func (pf *PrimeFactorOp) SquareRootUint64(number uint64) uint64 {

	return uint64(math.Sqrt(float64(number)))
}

// Calculating prime factors for big integers
type BigPrimeFactorOp struct {
	BaseNum      *big.Int
	FacsAndExpts [][][]*big.Int // Prime Factors and Exponents
}

func (bpf *BigPrimeFactorOp) PrimeFactorization(number *big.Int) error {

	bpf.FacsAndExpts = make([][][]*big.Int, 0)

	n := big.NewInt(0).Set(number)
	one := big.NewInt(1)
	limit := bpf.SquareRootPlus1(n)

	for i := big.NewInt(2); i.Cmp(limit) == -1; i = i.Add(i, one) {

		if bpf.IsBigModZero(n, i) {
			ct := big.NewInt(0)

			for bpf.IsBigModZero(n, i) == true {
				n = big.NewInt(0).Div(n, i)
				ct = big.NewInt(0).Add(ct, one)
			}

			limit = bpf.SquareRootPlus1(n)

			bpf.FacsAndExpts = append(bpf.FacsAndExpts, [][]*big.Int{{big.NewInt(0).Set(i), ct}})
		}
	}

	if n.Cmp(one) != 0 {
		bpf.FacsAndExpts = append(bpf.FacsAndExpts, [][]*big.Int{{n, one}})
	}

	return nil
}

func (bpf *BigPrimeFactorOp) IsBigModZero(dividend, divisor *big.Int) bool {

	mod := big.NewInt(0).Mod(dividend, divisor)

	if mod.Cmp(big.NewInt(0)) == 0 {
		return true
	}

	return false
}

func (bpf *BigPrimeFactorOp) SquareRootPlus1(number *big.Int) *big.Int {
	sqrt := big.NewInt(0).Sqrt(number)

	return sqrt.Add(sqrt, big.NewInt(1))
}
