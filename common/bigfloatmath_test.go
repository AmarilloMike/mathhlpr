package common

import (
	"math/big"
	"testing"
)

/*
	This source code file, 'bigfloatmath_test.go', is located in source code repository:

  		https://github.com/MikeAustin71/mathhlpr.git

	This test library is used to perform tests associated with the source code file,
 	'bigfloatmath.go'

*/

func TestBigFloatOp_PwrBig_01(t *testing.T) {

	power := big.NewInt(5)
	number := big.NewFloat(2.25)
	expected := "57.6650390625"
	result := BigFloatOp{}.NewPtr().PwrBig(number, power)
	resultStr := result.Text('f', 10)

	if expected != resultStr {
		t.Errorf("Error - Expected PwrBig result= %v .  Instead, result= %v", expected, resultStr)
	}

}

func TestBigFloatOp_PwrBig_02(t *testing.T) {

	power := big.NewInt(12)
	number := big.NewFloat(2.25)
	expected := "16834.1121960282"
	result := BigFloatOp{}.NewPtr().PwrBig(number, power)
	resultStr := result.Text('f', 10)
	if expected != resultStr {
		t.Errorf("Error - Expected PwrBig result= %v .  Instead, result= %v", expected, resultStr)
	}

}

func TestBigFloatOp_PwrBig_03(t *testing.T) {
	number := big.NewFloat(2.25)
	power := big.NewInt(-5)
	expected := "0.017341529915832609"
	precision := 18
	result := BigFloatOp{}.NewPtr().PwrBig(number, power)
	resultStr := result.Text('f', precision)
	if expected != resultStr {
		t.Errorf("Error - Expected PwrBig result= %v .  Instead, result= %v", expected, resultStr)
	}
}
func TestBigFloatOp_PwrBig_04(t *testing.T) {
	number := big.NewFloat(25.0)
	power := big.NewInt(-4)
	expected := "0.00000256"
	precision := 8

	result := BigFloatOp{}.NewPtr().PwrBig(number, power)
	resultStr := result.Text('f', precision)
	if expected != resultStr {
		t.Errorf("Error - Expected PwrBig result= %v .  Instead, result= %v", expected, resultStr)
	}
}

func TestBigFloatOp_PwrBig_05(t *testing.T) {
	number := big.NewFloat(5.0)
	power := big.NewInt(-4)
	expected := "0.0016"
	precision := 4

	result := BigFloatOp{}.NewPtr().PwrBig(number, power)
	resultStr := result.Text('f', precision)
	if expected != resultStr {
		t.Errorf("Error - Expected PwrBig result= %v .  Instead, result= %v", expected, resultStr)
	}
}
