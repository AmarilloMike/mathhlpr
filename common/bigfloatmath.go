package common

import (
	"math"
	"math/big"
)

/*
	This source code file, 'bigfloatmath.go', is located in source code repository:

  		https://github.com/MikeAustin71/mathhlpr.git

	NOTE: This source code file is still in development.

	This source file contains the structure and associated methods for 'BigFloatOp'.
	These methods perform math operations using the type *big.Float which is specific
	to 'golang'.

*/

type BigFloatOp struct {
	Input  *big.Float
	Result *big.Float
}

func (bf BigFloatOp) NewPtr() *BigFloatOp {
	bf2 := BigFloatOp{}

	return &bf2
}

// PwrBig - Raises *big.Float to a specified power or exponent.
// Uses the 'Exponentiation by squaring' algorithm. See:
// https://en.wikipedia.org/wiki/Exponentiation_by_squaring
// https://en.wikipedia.org/wiki/Exponentiation_by_squaring#Computation_by_powers_of_2
// This method is adapted from :
// https://stackoverflow.com/questions/30182129/calculating-large-exponentiation-in-golang
func (bf *BigFloatOp) PwrBig(base *big.Float, power *big.Int) *big.Float {

	tBase := big.NewFloat(0.0).Set(base)
	tPower := big.NewInt(0).Set(power)
	oneFloat := big.NewFloat(1.0)
	zeroFloat := big.NewFloat(0.0)
	zeroInt := big.NewInt(0)

	if tPower.Sign() == -1 {
		tPower = big.NewInt(0).Mul(power, big.NewInt(-1))
		tBase = big.NewFloat(0.0).Quo(oneFloat, tBase)
	}

	if tBase.Cmp(zeroFloat) == 0 {
		return zeroFloat
	}

	if power.Cmp(zeroInt) == 0 {
		return oneFloat
	}

	twoInt := big.NewInt(2)
	oneInt := big.NewInt(1)
	result := big.NewFloat(1.0)

	for tPower.Cmp(zeroInt) == 1 {
		temp := big.NewFloat(0)

		if big.NewInt(0).Mod(tPower, twoInt).Cmp(oneInt) == 0 {
			temp = big.NewFloat(0.0).Mul(result, tBase)
			result = big.NewFloat(0).Set(temp)
		}

		temp = big.NewFloat(0.0)
		temp.Mul(tBase, tBase)
		tBase = big.NewFloat(0).Set(temp)
		tPower = big.NewInt(0).Div(tPower, twoInt)
	}

	return result
}

func (bf *BigFloatOp) PwrInt64(number *big.Float, power int64) *big.Float {

	bigPower := big.NewInt(power)

	return bf.PwrBig(number, bigPower)

}

//
// See: https://www.codeproject.com/tips/311714/natural-logarithms-and-exponent
// Jacob F. W.
// Adapted from the original by John Gabriel
// Doesn't work
// Trying
// https://rosettacode.org/wiki/Nth_root#Go
func (bf *BigFloatOp) NthRootFloat64(number, nthRoot float64) float64 {

	A := number
	N := nthRoot
	S := float64(1.0)
	T := float64(0.0)
	L := float64(0.0)
	R := float64(0.0)
	one := float64(1.0)
	doLoop := true

	for doLoop == true {
		T = S
		L = (A / math.Pow(S, (N-one)))
		R = (N - one) * S
		S = (L + R) / N

		if L != S {
			doLoop = false
		}
	}

	if T > float64(5.999) {
		T = 0.0
	}

	return S
}
