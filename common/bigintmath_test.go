package common

import (
	"math/big"
	"testing"
)

/*
	This source code test file, 'bigintmath.go', is located in source code repository:

  https://github.com/MikeAustin71/mathhlpr.git

	These tests are used in conjunction with source code file 'bigintmath.go'
*/

func TestBigIntOp_PwrBySquares_01(t *testing.T) {
	base := big.NewInt(25)
	power := big.NewInt(4)
	expected := "390625"

	bio := BigIntOp{}

	result, err := bio.PwrBySquares(base, power)

	if err != nil {
		t.Errorf("Error thrown by bio.PwrBySquares(base, power). base= %v  power= %v  Error= %v", base, power, err)
	}

	resultStr := result.String()

	if expected != resultStr {
		t.Errorf("Expected result = %v  .  Instead result = %v . ", expected, resultStr)
	}

}

func TestBigIntOp_PwrBySquares_02(t *testing.T) {
	base := big.NewInt(-36)
	power := big.NewInt(5)
	expected := "-60466176"

	bio := BigIntOp{}

	result, err := bio.PwrBySquares(base, power)

	if err != nil {
		t.Errorf("Error thrown by bio.PwrBySquares(base, power). base= %v  power= %v  Error= %v", base, power, err)
	}

	resultStr := result.String()

	if expected != resultStr {
		t.Errorf("Expected result = %v  .  Instead result = %v . ", expected, resultStr)
	}

}

func TestBigIntOp_PwrBySquares_03(t *testing.T) {
	base := big.NewInt(5)
	power := big.NewInt(30)
	expected := "931322574615478515625"

	bio := BigIntOp{}

	result, err := bio.PwrBySquares(base, power)

	if err != nil {
		t.Errorf("Error thrown by bio.PwrBySquares(base, power). base= %v  power= %v  Error= %v", base, power, err)
	}

	resultStr := result.String()

	if expected != resultStr {
		t.Errorf("Expected result = %v  .  Instead result = %v . ", expected, resultStr)
	}

}
