package common

import (
	"fmt"
	"math/big"
)

/*
	This source code file, 'bigintmath.go', is located in source code repository:

  https://github.com/MikeAustin71/mathhlpr.git

	NOTE: This source code file is still in development.

	This source file contains the structure and associated methods for 'BigIntOp'.
	These methods perform math operations using *big.Int types specific to 'golang'.

*/

type BigIntOp struct {
	Input  *big.Int
	Result *big.Int
}

func (bi BigIntOp) NewPtr() *BigIntOp {
	bi2 := BigIntOp{}

	return &bi2
}

// PwrBySquares - raises a *big.Int input parameter, 'base' to a
// value of 'power'.
// Input Parameters
// base = the number which will be raised to the designated power.
// power = the power or exponent by which 'number' will be raised.
//   				'power' must be >= zero. If 'power' is less than zero
//           an error will be thrown
//
// The algorithm used to raise 'base' to a value of 'power' is named,
// 'Exponentiation by squaring'. See:
// https://en.wikipedia.org/wiki/Exponentiation_by_squaring
// https://en.wikipedia.org/wiki/Exponentiation_by_squaring#Computation_by_powers_of_2
// This method is based on revised code taken in part from Ye Lin Aung and Salvador Dali .
// https://stackoverflow.com/questions/30182129/calculating-large-exponentiation-in-golang
func (bi *BigIntOp) PwrBySquares(base, power *big.Int) (*big.Int, error) {

	tPower := big.NewInt(0).Set(power)
	tBase := big.NewInt(0).Set(base)
	one := big.NewInt(1)
	zero := big.NewInt(0)
	two := big.NewInt(2)
	result := big.NewInt(0).Set(one)

	if base.Cmp(zero) == 0 {

		return zero, nil
	}

	if tPower.Cmp(two) == -1 {

		if tPower.Cmp(one) == 0 {
			return result, nil
		}

		if tPower.Cmp(zero) == 0 {
			return one, nil
		}

		if tPower.Cmp(zero) == -1 {
			return zero, fmt.Errorf("Error: Input parameter 'power' is negative. Only positive values for 'power' can be processed. 'power'= %v", power)
		}

	}

	for tPower.Cmp(zero) == 1 {
		temp := big.NewInt(0)

		if big.NewInt(0).Mod(tPower, two).Cmp(one) == 0 {
			temp = big.NewInt(0).Mul(result, tBase)
			result = big.NewInt(0).Set(temp)
		}

		temp = big.NewInt(0)
		temp.Mul(tBase, tBase)
		tBase = big.NewInt(0).Set(temp)
		tPower = big.NewInt(0).Div(tPower, two)
	}

	return result, nil
}

func (bi *BigIntOp) PowExp(base, power *big.Int) *big.Int {

	zero := big.NewInt(0)
	result := big.NewInt(0).Exp(base, power, zero)

	return result
}

func (bi *BigIntOp) PowBig(base, power *big.Int) *big.Int {

	tmp := big.NewInt(0).Set(base)
	tPower := big.NewInt(0).Set(power)
	res := big.NewInt(1)
	zero := big.NewInt(0)
	two := big.NewInt(2)
	one := big.NewInt(1)

	for tPower.Cmp(zero) == 1 {
		temp := new(big.Int)
		if big.NewInt(0).Mod(tPower, two).Cmp(one) == 0 {
			temp.Mul(res, tmp)
			res = temp
		}
		temp = new(big.Int)
		temp.Mul(tmp, tmp)
		tmp = temp
		tPower = big.NewInt(0).Div(tPower, two)
	}

	return res
}

// SquareRoot - Returns the square root of a number of type *big.Int.
// Input parameter 'number' must be >= zero. If 'number' is less than
// zero the method will throw a 'panic' error.
func (bi *BigIntOp) SquareRoot(number *big.Int) *big.Int {

	return big.NewInt(0).Sqrt(number)

}

// SquareRootPlus1 - Returns the square root of input parameter
// 'number', plus one. This value is useful in *big.Int loops.
// The input parameter 'number' must be >= zero. If 'number' is
// less than zero, the method will throw a 'panic' error.
func (bi *BigIntOp) SquareRootPlus1(number *big.Int) *big.Int {

	sqrt := big.NewInt(0).Sqrt(number)

	return sqrt.Add(sqrt, big.NewInt(1))
}

func (bi *BigIntOp) IsPrime(number *big.Int) bool {

	if number.Cmp(big.NewInt(0)) == -1 {
		return false
	}

	if number.Cmp(big.NewInt(5)) == -1 {

		if number.Cmp(big.NewInt(0)) == 0 {
			return false
		}

		if number.Cmp(big.NewInt(1)) == 0 {
			return false
		}

		if number.Cmp(big.NewInt(2)) == 0 {
			return true
		}

		if number.Cmp(big.NewInt(3)) == 0 {
			return true
		}

		if number.Cmp(big.NewInt(4)) == 0 {
			return false
		}
	}

	limit := big.NewInt(0).Sqrt(number)
	limit.Add(limit, big.NewInt(1))
	one := big.NewInt(1)
	mod := big.NewInt(0)
	zero := big.NewInt(0)
	for i := big.NewInt(2); i.Cmp(limit) == -1; i = big.NewInt(0).Add(i, one) {

		mod = big.NewInt(0).Mod(number, i)

		if mod.Cmp(zero) == 0 {
			return false
		}

	}

	return true

}

func (bi *BigIntOp) FindPrimes(number *big.Int) ([]*big.Int, error) {
	primes := make([]*big.Int, 0)

	return primes, nil
}
