package common

import (
	"math/big"
	"testing"
)

/*
    This source file is located in source code repository:
          https://github.com/MikeAustin71/mathhlpr.git

	This source code file contains tests associated with structure
	'BitPrimeFactorOp' which is located in source file, 'primefactors.go'

*/

func TestBigPrimeFactorOp_PrimeFactorization_01(t *testing.T) {

	number := big.NewInt(64)
	expectedPrime := big.NewInt(2)
	expectedExponent := big.NewInt(6)
	bpf := BigPrimeFactorOp{}

	bpf.PrimeFactorization(number)

	if bpf.FacsAndExpts[0][0][0].Cmp(expectedPrime) != 0 {
		t.Errorf("Expected Prime Number= %v .   Instead Prime Number= %v", expectedPrime, bpf.FacsAndExpts[0][0][0])
	}

	if bpf.FacsAndExpts[0][0][1].Cmp(expectedExponent) != 0 {
		t.Errorf("Expected Prime Exponent= %v .   Instead Prime Exponent= %v", expectedExponent, bpf.FacsAndExpts[0][0][1])
	}

}

func TestBigPrimeFactorOp_PrimeFactorization_02(t *testing.T) {

	number := big.NewInt(124)
	expected := make([][][]*big.Int, 0)
	expected = append(expected, [][]*big.Int{{big.NewInt(2), big.NewInt(2)}})
	expected = append(expected, [][]*big.Int{{big.NewInt(31), big.NewInt(1)}})

	/*
		i=  0   Prime =  2   Count =  2
		i=  1   Prime =  31   Count =  1
	*/

	bpf := BigPrimeFactorOp{}

	bpf.PrimeFactorization(number)

	for i, x := range expected {

		if x[0][0].Cmp(bpf.FacsAndExpts[i][0][0]) != 0 {
			t.Errorf("Prime Numbers Don't Match. Expected Prime= %v . Instead Received Exponent= %v", x[0][0], bpf.FacsAndExpts[i][0][0])
		}

		if x[0][1].Cmp(bpf.FacsAndExpts[i][0][1]) != 0 {
			t.Errorf("Prime Exponents Don't Match. Expected Exponent= %v . Instead Exponent= %v", x[0][1], bpf.FacsAndExpts[i][0][1])
		}
	}

}

func TestBigPrimeFactorOp_PrimeFactorization_03(t *testing.T) {
	// Testing factorization of a prime number
	number := big.NewInt(67)
	expected := make([][][]*big.Int, 0)
	expected = append(expected, [][]*big.Int{{big.NewInt(67), big.NewInt(1)}})

	/*
		i=  0   Prime =  67   Count =  1
	*/

	bpf := BigPrimeFactorOp{}

	bpf.PrimeFactorization(number)

	for i, x := range expected {

		if x[0][0].Cmp(bpf.FacsAndExpts[i][0][0]) != 0 {
			t.Errorf("Prime Numbers Don't Match. Expected Prime= %v . Instead Received Exponent= %v", x[0][0], bpf.FacsAndExpts[i][0][0])
		}

		if x[0][1].Cmp(bpf.FacsAndExpts[i][0][1]) != 0 {
			t.Errorf("Prime Exponents Don't Match. Expected Exponent= %v . Instead Exponent= %v", x[0][1], bpf.FacsAndExpts[i][0][1])
		}
	}

}

func TestPrimeFactorOp_PrimeFactorization_01(t *testing.T) {
	number := uint64(64)
	expectedPrime := uint64(2)
	expectedExponent := uint64(6)
	pf := PrimeFactorOp{}

	pf.PrimeFactorization(number)

	if pf.FacsAndExpts[0][0][0] != expectedPrime {
		t.Errorf("Expected Prime Number= %v .   Instead Prime Number= %v", expectedPrime, pf.FacsAndExpts[0][0][0])
	}

	if pf.FacsAndExpts[0][0][1] != expectedExponent {
		t.Errorf("Expected Prime Exponent= %v .   Instead Prime Exponent= %v", expectedExponent, pf.FacsAndExpts[0][0][1])
	}

}

func TestPrimeFactorOp_PrimeFactorization_02(t *testing.T) {

	number := uint64(124)
	expected := make([][][]uint64, 0)
	expected = append(expected, [][]uint64{{2, 2}})
	expected = append(expected, [][]uint64{{31, 1}})

	/*
		i=  0   Prime =  2   Count =  2
		i=  1   Prime =  31   Count =  1
	*/

	pf := PrimeFactorOp{}

	pf.PrimeFactorization(number)

	for i, x := range expected {

		if x[0][0] != pf.FacsAndExpts[i][0][0] {
			t.Errorf("Prime Numbers Don't Match. Expected Prime= %v . Instead Received Exponent= %v", x[0][0], pf.FacsAndExpts[i][0][0])
		}

		if x[0][1] != pf.FacsAndExpts[i][0][1] {
			t.Errorf("Prime Exponents Don't Match. Expected Exponent= %v . Instead Exponent= %v", x[0][1], pf.FacsAndExpts[i][0][1])
		}
	}

}

func TestPrimeFactorOp_PrimeFactorization_03(t *testing.T) {

	// Testing factorization of a prime number

	number := uint64(67)
	expected := make([][][]uint64, 0)
	expected = append(expected, [][]uint64{{67, 1}})

	/*
		i=  0   Prime =  67   Count =  1
	*/

	pf := PrimeFactorOp{}

	pf.PrimeFactorization(number)

	for i, x := range expected {

		if x[0][0] != pf.FacsAndExpts[i][0][0] {
			t.Errorf("Prime Numbers Don't Match. Expected Prime= %v . Instead Received Exponent= %v", x[0][0], pf.FacsAndExpts[i][0][0])
		}

		if x[0][1] != pf.FacsAndExpts[i][0][1] {
			t.Errorf("Prime Exponents Don't Match. Expected Exponent= %v . Instead Exponent= %v", x[0][1], pf.FacsAndExpts[i][0][1])
		}
	}

}
