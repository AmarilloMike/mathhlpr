package common

import (
	"fmt"
	"math/big"
)

/*
	This source code file, 'combinationspermutations.go', is located in source code repository:

  https://github.com/MikeAustin71/mathhlpr.git

	NOTE: This source code file is still in development.

	This source file contains the structure and associated methods for 'BigRatOp'.
	These methods perform math operations using *big.Rat types specific to 'golang'.

*/

type BigRatOp struct {
	Input       *big.Rat
	Output      *big.Rat
	Numerator   *big.Int
	Denominator *big.Int
}

func (bro *BigRatOp) GetNumeratorDenominator(fraction *big.Float, precision uint) (*big.Int, *big.Int, error) {
	rat, ok := big.NewRat(1, 1).SetString(fraction.Text('f', int(precision)))

	if !ok {
		return big.NewInt(0), big.NewInt(0), fmt.Errorf("Error returned from big.NewRat(1,1).SetString(fraction.Text('f',int(precision))). fraction= %v, precision= %v ", fraction, precision)
	}

	return rat.Num(), rat.Denom(), nil
}

func (bro *BigRatOp) GetRat(numerator, denominator *big.Int) *big.Rat {
	return big.NewRat(1, 1).SetFrac(numerator, denominator)
}

func (bro *BigRatOp) PwrByTwoV2(number *big.Rat, power *big.Int) *big.Rat {

	tBase := big.NewRat(1, 1).Set(number)
	tPower := big.NewInt(0).Set(power)
	oneRat := big.NewRat(1, 1)
	zeroInt := big.NewInt(0)

	if tPower.Sign() == -1 {
		tPower = big.NewInt(0).Mul(power, big.NewInt(-1))
		tBase = big.NewRat(1, 1).Quo(oneRat, tBase)
	}

	if power.Cmp(zeroInt) == 0 {
		return oneRat
	}

	twoInt := big.NewInt(2)
	oneInt := big.NewInt(1)
	result := big.NewRat(1, 1)
	cycle := 0
	for tPower.Cmp(zeroInt) == 1 {
		cycle++
		temp := big.NewRat(0, 1)

		if big.NewInt(0).Mod(tPower, twoInt).Cmp(oneInt) == 0 {
			temp = big.NewRat(1, 1).Mul(result, tBase)
			result = big.NewRat(1, 1).Set(temp)

			if tPower.Cmp(oneInt) == 0 {
				return result
			}
		}

		temp = big.NewRat(0, 1)
		temp.Mul(tBase, tBase)
		tBase = big.NewRat(1, 1).Set(temp)
		tPower = big.NewInt(0).Div(tPower, twoInt)
	}

	return result

}

/*

func (bro *BigRatOp) PwrBySquares(base *big.Rat, power int) *big.Rat {

	//tBase := big.NewRat(1, 1).Set(base)
	//tPower := big.NewInt(0).Set(power)
	remainPwr := power
	consumedPwr := 1

	//oneRat := big.NewRat(1, 1)
	zeroInt := 0
	twoInt := 2
	//oneInt := 1
	result := big.NewRat(1, 1)
	multiplier := big.NewRat(1, 1).Set(base)
	pwrIncrement := 1
	nextPwrIncrement := 1

	// assume power greater than or equal to two
	for remainPwr > zeroInt {

		result.Mul(result, multiplier)

		consumedPwr = consumedPwr + pwrIncrement

		remainPwr = power - consumedPwr

		nextPwrIncrement = pwrIncrement * twoInt

		if nextPwrIncrement > remainPwr {
			multiplier.Set(base)
			pwrIncrement = 1
			limit := remainPwr / 2
			for i := 0; i < limit; i++ {
				pwrIncrement++
				multiplier.Mul(multiplier, base)
			}

		} else {

			multiplier.Set(result)
			pwrIncrement = pwrIncrement * twoInt

		}

	}

	return result
}

func (bro *BigRatOp) PwrBySquares(base *big.Rat, power *big.Int) *big.Rat {

	tBase := big.NewRat(1, 1).Set(base)
	//tPower := big.NewInt(0).Set(power)
	remainPwr := big.NewInt(0).Set(power)
	consumedPwr := big.NewInt(1)

	//oneRat := big.NewRat(1, 1)
	zeroInt := big.NewInt(0)
	twoInt := big.NewInt(2)
	oneInt := big.NewInt(1)
	result := big.NewRat(1, 1).Set(base)

	// assume power greater than or equal to two
	for remainPwr.Cmp(zeroInt) == 1 {

		if remainPwr.Cmp(consumedPwr) == -1 {

			result.Mul(result, tBase)
			consumedPwr.Add(consumedPwr, oneInt)

		} else {

			result.Mul(result, result)
			consumedPwr.Mul(consumedPwr, twoInt)

		}

		remainPwr = remainPwr.Sub(power, consumedPwr)

	}

	return result
}
*/

func (bro *BigRatOp) PwrByTwo(number *big.Rat, power *big.Int) *big.Rat {

	tBase := big.NewRat(1, 1).Set(number)
	tPower := big.NewInt(0).Set(power)
	oneRat := big.NewRat(1, 1)
	zeroInt := big.NewInt(0)

	if tPower.Sign() == -1 {
		tPower = big.NewInt(0).Mul(power, big.NewInt(-1))
		tBase = big.NewRat(1, 1).Quo(oneRat, tBase)
	}

	if power.Cmp(zeroInt) == 0 {
		return oneRat
	}

	twoInt := big.NewInt(2)
	oneInt := big.NewInt(1)
	result := big.NewRat(1, 1)
	cycle := 0
	for tPower.Cmp(zeroInt) == 1 {
		cycle++
		temp := big.NewRat(0, 1)

		if big.NewInt(0).Mod(tPower, twoInt).Cmp(oneInt) == 0 {
			temp = big.NewRat(1, 1).Mul(result, tBase)
			result = big.NewRat(1, 1).Set(temp)
		}

		temp = big.NewRat(0, 1)
		temp.Mul(tBase, tBase)
		tBase = big.NewRat(1, 1).Set(temp)
		tPower = big.NewInt(0).Div(tPower, twoInt)
	}

	return result

}
