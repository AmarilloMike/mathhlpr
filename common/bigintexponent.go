package common

import (
	"fmt"
	"math/big"
)

/*
	This source code file, 'bigintexponent.go', is located in source code repository:

  		https://github.com/MikeAustin71/mathhlpr.git

	NOTE: This source code file is still in development.

	This source file contains the structure and associated methods for 'BigIntExpOp'.
	These methods perform math operations using large exponents.

*/

/*
https://stackoverflow.com/questions/30182129/calculating-large-exponentiation-in-golang
https://en.wikipedia.org/wiki/Exponentiation_by_squaring#Computation_by_powers_of_2
*/

type BigIntExpOp struct {
	Input  *big.Int
	Output *big.Int
	Power  *big.Int
}

func (bie *BigIntExpOp) Exp(base, power *big.Int, precision int) (*big.Int, *big.Int) {
	newPrecison := big.NewInt(0).Mul(power, big.NewInt(int64(precision)))
	result := big.NewInt(0).Exp(base, power, big.NewInt(0))

	return result, newPrecison
}

// ExpByPowOfTwo - Raises a *big.Int 'base', to the specified 'power'
// using the Exponentiation by squaring algorithm.
// See:
// https://en.wikipedia.org/wiki/Exponentiation_by_squaring
// https://en.wikipedia.org/wiki/Exponentiation_by_squaring#Computation_by_powers_of_2
// This method is based on revised code taken in part from Ye Lin Aung.
// https://stackoverflow.com/questions/30182129/calculating-large-exponentiation-in-golang
func (bie *BigIntExpOp) ExpByPowOfTwo(base, power *big.Int) (*big.Int, error) {
	tPower := big.NewInt(0).Set(power)
	tBase := big.NewInt(0).Set(base)
	one := big.NewInt(1)
	zero := big.NewInt(0)
	two := big.NewInt(2)
	result := big.NewInt(0).Set(one)

	if base.Cmp(zero) == 0 {

		return zero, nil
	}

	if tPower.Cmp(two) == -1 {

		if tPower.Cmp(one) == 0 {
			return result, nil
		}

		if tPower.Cmp(zero) == 0 {
			return one, nil
		}

		if tPower.Cmp(zero) == -1 {
			return zero, fmt.Errorf("Error: Input parameter 'power' is negative. Only positive values for 'power' can be processed. 'power'= %v", power)
		}

	}

	for tPower.Cmp(zero) == 1 {
		temp := big.NewInt(0)

		if big.NewInt(0).Mod(tPower, two).Cmp(one) == 0 {
			temp = big.NewInt(0).Mul(result, tBase)
			result = big.NewInt(0).Set(temp)
		}

		temp = big.NewInt(0)
		temp.Mul(tBase, tBase)
		tBase = big.NewInt(0).Set(temp)
		tPower = big.NewInt(0).Div(tPower, two)
	}

	return result, nil
}

func (bie *BigIntExpOp) modBy2(x *big.Int) *big.Int {
	return big.NewInt(0).Mod(x, big.NewInt(2))
}

func (bie *BigIntExpOp) divideBy2(x *big.Int) *big.Int {
	return big.NewInt(0).Div(x, big.NewInt(2))
}

func (bie *BigIntExpOp) multiply(x, y *big.Int) *big.Int {
	return big.NewInt(0).Mul(x, y)
}
