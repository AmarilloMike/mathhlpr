package common

import (
	"math/big"
)

/*
	This source code file, 'commonfactors.go', is located in source code repository:

  https://github.com/MikeAustin71/mathhlpr.git

	NOTE: This source code file is still in development.

*/

type GreatestCommonDivisor struct {
	Numerator   *big.Int
	Denominator *big.Int
	Gcd         *big.Int
}

func (gcd *GreatestCommonDivisor) GetGcd(num1, num2 *big.Int) GreatestCommonDivisor {

	gcd1 := GreatestCommonDivisor{}

	cmpResult := num1.Cmp(num2)

	switch cmpResult {

	case 1:
		gcd1.Numerator = num2
		gcd1.Denominator = num1
		break
	case -1:
		gcd1.Numerator = num1
		gcd1.Denominator = num2
		break
	case 0:
		gcd1.Numerator = num1
		gcd1.Denominator = num2
		gcd1.Gcd = big.NewInt(0).Set(num1)
		return gcd1
	}

	dividend := big.NewInt(0).Set(gcd1.Denominator)
	divisor := big.NewInt(0).Set(gcd1.Numerator)
	remainder := big.NewInt(1)
	lastRemainder := big.NewInt(0).Set(divisor)
	bigZero := big.NewInt(0)

	remainder.Set(divisor)

	for remainder.Cmp(bigZero) != 0 {
		lastRemainder.Set(remainder)
		remainder = big.NewInt(0).Mod(dividend, divisor)
		dividend.Set(divisor)
		divisor.Set(remainder)
	}

	gcd1.Gcd = lastRemainder

	return gcd1
}
