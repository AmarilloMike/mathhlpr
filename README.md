# mathhlpr

Helper Libraries used in performing specific math operations.
Currently supported operations include extracting the nth Roots and Square Roots of real
numbers.

The source code for square root and nth root extraction is maintained
in the source code file, 'nthroot.go'.

Source code is written in the 'go' programming language (a.k.a 'golang').

This library may be cloned from source code repository:

 https://bitbucket.org/AmarilloMike/mathhlpr/src


### Dependencies:
 This library is dependent on the IntAry structure which
 is contained in the source code file, 'intary.go'.

 IntAry is maintained as part of a separate library which
 may be cloned from:

 https://bitbucket.org/AmarilloMike/intary/src

### NOTES
nthroot.go and primefactors.go
