package main

import (
	"MikeAustin71/mathhlpr/common"
	"fmt"
	"math/big"
	"time"
)

/*

import (
	"math/big"
	"MikeAustin71/mathhlpr/common"
	"fmt"
	"time"
)

2:43
*/

func main() {

	n := big.NewInt(5000)
	precision := 51
	expected := "2.71827"
	EulersNo4(n, precision, expected)
}

func EulersNo4(n *big.Int, precision int, expected string) {
	// 2.71828182845904523536028747135266249775724709369995
	// expected :=       "2.71828182845904523536028747135266249775724709369995"
	// result :=          2.7182804693193768838197997084543563927516450266825077
	// 1-Hours 34-Minutes 58-Seconds 176-Milliseconds 253-Microseconds 700-Nanoseconds

	lop := common.LogOp{}
	result := lop.GetEulersNum4(n)
	resultStr := result.FloatString(precision)
	fmt.Println("=== Euler's Number 4 ===")
	fmt.Println("            n: ", n)
	fmt.Println("       result: ", resultStr)
	fmt.Println("     expected: ", expected)

}

func BigIntPowByTwo(number, power *big.Int, expected string) {

	// result: 390625
	// expected:  390625
	startTime := time.Now()
	tenInt := big.NewInt(10)
	sixmil := big.NewInt(6000000)
	result1 := big.NewInt(0).Exp(number, power, big.NewInt(0))
	denominator := big.NewInt(0).Exp(tenInt, sixmil, big.NewInt(0))

	result2 := big.NewRat(1, 1).SetFrac(result1, denominator)

	endTime := time.Now()

	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)

	fmt.Println("=== BigIntPowByTwo ===")
	fmt.Println("    number: ", number)
	fmt.Println("     power: ", power)
	// fmt.Println("  result2: ", result2)
	fmt.Println("result2-52: ", result2.FloatString(52))
	fmt.Println("  expected: ", expected)

	fmt.Println("---------------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)
	fmt.Println("=================================")

}

func RatPwr(base *big.Rat, power *big.Int, expected string, basePrecision, resultPrecision int) {

	br := common.BigRatOp{}

	startTime := time.Now()

	result := br.PwrByTwoV2(base, power)

	endTime := time.Now()

	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)
	resultStr := result.FloatString(resultPrecision)
	fmt.Println("=== Big Rat PwrByTwo ===")
	fmt.Println("          base: ", base.FloatString(basePrecision))
	fmt.Println("         power: ", power)
	fmt.Println(" result ratStr: ", result.RatString())
	fmt.Println(" result String: ", result.String())
	fmt.Println("        result: ", resultStr)
	fmt.Println("      expected: ", expected)
	fmt.Println("---------------------------------------")
	if expected != resultStr {
		fmt.Println("ERROR - Expected NOT EQUAL to result!")
	} else {
		fmt.Println(" SUCCESS - Expected EQUAl to result!")
	}

	fmt.Println("---------------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)
	fmt.Println("=================================")

}

func EulersNo3(n *big.Int) {
	// 2.71828182845904523536028747135266249775724709369995
	// expected :=       "2.71828182845904523536028747135266249775724709369995"
	// result :=          2.7182804693193768838197997084543563927516450266825077
	// 1-Hours 34-Minutes 58-Seconds 176-Milliseconds 253-Microseconds 700-Nanoseconds

	lop := common.LogOp{}
	result := lop.GetEulersNum3(n)

	fmt.Println("=== Euler's Number 3 ===")
	fmt.Println("            n: ", n)
	fmt.Println("       result: ", result.GetNumStr())
	fmt.Println("     expected: ", "271828182845904523536028747135266249775724709369995")

}

func RatPwrAdd(base *big.Rat, power *big.Int) {
	br := common.BigRatOp{}

	result1 := br.PwrByTwo(base, power)
	result1Txt := result1.FloatString(10000)
	result1abrv, ok := big.NewRat(1, 1).SetString(result1Txt)

	if !ok {
		fmt.Println("big.NewRat(1,1).SetString(result1Txt) failed!")
		return
	}

	result3 := big.NewRat(1, 1).Mul(result1abrv, result1abrv)

	fmt.Println("=== Big Rat PwrByTwo ===")
	fmt.Println("     base: ", base.FloatString(52))
	fmt.Println("    power: ", power)
	//fmt.Println("   result:", result.RatString())
	fmt.Println("   result:", result3.FloatString(52))

}

func BigIntExp(number, power *big.Int, precision int, expected string) {

	bie := common.BigIntExpOp{}
	result, newPrecision := bie.Exp(number, power, precision)

	fmt.Println("=== BigIntPowByTwo ===")
	fmt.Println("   number: ", number)
	fmt.Println("    power: ", power)
	fmt.Println("   result:", result)
	fmt.Println(" expected:", expected)
	fmt.Println("precision: ", newPrecision)

}

func PwrFloat(base *big.Float, power *big.Int, expected string, precision int) {
	// Blows-Up
	//base := big.NewFloat(0.00000000000000000000000025)
	//power := big.NewInt(50000)
	//expected := "0.00000000000000000000000025"
	//precision := 250000

	bf := common.BigFloatOp{}
	result := bf.PwrBig(base, power)
	resultStr := result.Text('f', precision)

	fmt.Println("=== BigFloat PowBig ===")
	fmt.Println("    base: ", base)
	fmt.Println("   power: ", power)
	fmt.Println("  result:", resultStr)
	fmt.Println("expected: ", expected)

	if expected != resultStr {
		fmt.Println("result DOES NOT EQUAL expected result.")
	} else {
		fmt.Println("Success! Result equals expected result")
	}

}

func PowerBySquares(base, power *big.Int, expected string) {

	//base:= big.NewInt(-2)
	//power:=  big.NewInt(13)
	//expected:=  "-8192"

	bio := common.BigIntOp{}

	result, _ := bio.PwrBySquares(base, power)

	fmt.Println("=== PowBig ===")
	fmt.Println("    base: ", base)
	fmt.Println("   power: ", power)
	fmt.Println("  result:", result)
	fmt.Println("expected: ", expected)
	if expected != result.String() {
		fmt.Println("result DOES NOT EQUAL expected result.")
	} else {
		fmt.Println("Success! Result equals expected result")
	}

}

func EulersNo2(n *big.Int, expected string, precision int) {

	lop := common.LogOp{}

	result, err := lop.GetEulersNum2(n)

	if err != nil {
		fmt.Printf("Error returned from lop.GetEulersNum2(big.NewInt(n)). err= %v \n", err)
		return
	}

	fmt.Println("=== Euler's Number 2 ===")
	fmt.Println("            n: ", n)
	fmt.Println("       result: ", result.Text('f', precision))
	fmt.Println("     expected: ", expected)

}

func EulersNumber(cycles *big.Int, expected string, precision int) {

	lop := common.LogOp{}

	result := lop.GetEulersNum(cycles)

	fmt.Println("=== Euler's Number ===")
	fmt.Println("       cycles: ", cycles)
	fmt.Println("       result: ", result.Text('f', precision))
	fmt.Println("     expected: ", expected)

}

func AreaHyperTangent(z *big.Float, cycles int64, expected string) {

	lop := common.LogOp{}

	result := lop.AreaHyperTangent(z, cycles)

	fmt.Println("=== AreaHyperTangent ===")
	fmt.Println("            z: ", z.Text('f', 5))
	fmt.Println("       cycles: ", cycles)
	fmt.Println("       result: ", result.Text('f', 15))
	fmt.Println("     expected: ", expected)

}

/*
func TaylorSeries(z *big.Float, cycles int64, expected string) {

	lop := common.LogOp{}

	result := lop.TaylorSeries(z, cycles)

	fmt.Println("=== Taylor Series ===")
	fmt.Println("            z: ", z.Text('f', 5) )
	fmt.Println("       cycles: ", cycles)
	fmt.Println("       result: ", result.Text('f', 17))
	fmt.Println("     expected: ", expected)


}

func GetRat() {

	numerator := big.NewInt(1)
	denominator := big.NewInt(3)
	bro := common.BigRatOp{}
  rat := bro.GetRat(numerator, denominator)

	fmt.Println("=== GetRat ===")
	fmt.Println("    numerator: ", rat.Num())
	fmt.Println("  denominator: ", rat.Denom())
	fmt.Println(" Float String: ", rat.FloatString(10))

}

func RatNumber() {

	bro := common.BigRatOp{}
	bFloat := big.NewFloat(7.)
	precision := 10
	numerator, denominator, err := bro.GetNumeratorDenominator(bFloat, uint(precision))

	if err != nil {
		fmt.Printf("Error returned from bro.GetNumeratorDenominator(bFloat, uint(precision)). bFloat= %v precision= %v Error= %v \n", bFloat, precision, err)
		return
	}

	fmt.Println("=== RatNumber ===")
	fmt.Println("    numerator: ", numerator)
	fmt.Println("  denominator: ", denominator)
}

func NthRootFloat(number, nthRoot float64, expected string, precision int) {

	//number := float64(25)
	//nthRoot := float64(4)
	//expected := "2.23606797749979"

	bf := common.BigFloatOp{}
	result := bf.NthRootFloat64(number, nthRoot)
	resultStr := strconv.FormatFloat(result, 'f', precision, 32)

	fmt.Println("=== BigFloat PowBig ===")
	fmt.Println("    number: ", number)
	fmt.Println("   nthRoot: ", nthRoot)
	fmt.Println("  result:" , resultStr)
	fmt.Println("expected: ", expected)

}


func PowBig(base, power *big.Int, expected string ) {

	//base:= big.NewInt(5)
	//power:=  big.NewInt(30)
	//expected:=  "931322574615478515625"

	bio := common.BigIntOp{}

	result := bio.PowBig(base , power)

	fmt.Println("=== PowBig ===")
	fmt.Println("    base: ", base)
	fmt.Println("   power: ", power)
	fmt.Println("  result:" , result)
	fmt.Println("expected: ", expected)
	if expected != result.String(){
		fmt.Println("result DOES NOT EQUAL expected result.")
	} else {
		fmt.Println("Success! Result equals expected result")
	}


}

func DoFloatPowerBig(number *big.Float, power *big.Int, expectedResult string, precision int) {


		// number := big.NewFloat(2.25)
		// power := big.NewInt(-5)
		// expected := "0.017341529915832612"
		// precision := 18


	bfo := common.BigFloatOp{}

	result := bfo.PwrBig(number, power)
	resultStr := result.Text('f', precision)

	fmt.Println("=== DoFloatPowerBig ===")
	fmt.Println("         number: ", number)
	fmt.Println("          power: ", power)
	fmt.Println("         result: ", result)
	fmt.Println("      resultStr: ", resultStr)
	fmt.Println("Expected Result: ", expectedResult )
}

func DoAltHarmonics() {

	lop := common.LogOp{}

	result := lop.AltHarmonics()

	fmt.Println("Result - Alternating Harmonics Series")
	fmt.Println("result: ", result)

}


func DoBigPrimeFactor(number *big.Int) {

	bpf := common.BigPrimeFactorOp{}

	bpf.PrimeFactorization(number)
	fmt.Println("---------------------------")
	fmt.Println("    Big Prime Factor")
	for i, x := range bpf.FacsAndExpts {

		fmt.Println("i= ", i, "  Prime = ", x[0][0], "  Count = ", x[0][1])

	}

}


func DoPrimeFactor(number uint64) {

	pf := common.PrimeFactorOp{}

	pf.PrimeFactorization(number)
	fmt.Println("---------------------------")
	fmt.Println("   Uint64 Prime Factor")

	for i, x := range pf.FacsAndExpts {

		fmt.Println("i= ", i, "  Prime = ", x[0][0], "  Count = ", x[0][1])

	}

}


func IsBigPrime() {
	number := big.NewInt(999392147)
	bi := common.BigIntOp{}

	result := bi.IsPrime(number)
	fmt.Println("         number: ", number)
	fmt.Println(" IsPrime Result: ", result)

}

func BigExp() {
	number := big.NewInt(20)
	power := big.NewInt(40)

	bi := common.BigIntOp{}

	result, mod, _ := bi.Power(number, power)

	fmt.Println("number: ", number)
	fmt.Println(" power: ", power)
	fmt.Println("result: ", result)
	fmt.Println("   mod: ", mod)

}




func DoIsPrime(number uint64) {

	pf := common.PrimeFactorOp{}

	fmt.Println("  number: ", number)
	fmt.Println(" isPrime: ", pf.IsNumPrimeUint64(number))
}


func SqrRoot(number uint64) {

	pf := common.PrimeFactorOp{}

	fmt.Println("number: ", number)
	fmt.Println("sq-root int: ", pf.SquareRootUint64(number))


}


func DoGcd() {

	gcd := common.GreatestCommonDivisor{}

	num1 := big.NewInt(3)
	num2 := big.NewInt(6)

	gcd2 := gcd.GetGcd(num1, num2)

	fmt.Println("              Numerator: ", gcd2.Numerator.String())
	fmt.Println("            Denominator: ", gcd2.Denominator.String())
	fmt.Println("Greatest Common Divisor: ", gcd2.Gcd.String())
}



func DoNthRoot() {

	nRt := common.NthRootOp{}
	originalNum := common.IntAry{}.New()
	numStr1 := "125"
	nthRoot := uint(5)
	maxPrecision := uint(14)
	expected := "2.62652780440377"
	originalNum.SetIntAryWithNumStr(numStr1)
	startTime := time.Now()
	result, _ := nRt.GetNthRootIntAry(&originalNum, nthRoot, maxPrecision)

	endTime := time.Now()

	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)

	fmt.Println("Original Number: ", numStr1)
	fmt.Println("       Nth Root: ", nthRoot)
	fmt.Println("  Max Precision: ", maxPrecision)
	fmt.Println("---------------------------------")
	fmt.Println("BundlesArray")
	fmt.Println(nRt.BaseNumBundles)

	fmt.Println("---------------------------------")
	fmt.Println("Expected Result")
	fmt.Println(expected)

	fmt.Println("---------------------------------")
	fmt.Println("Actual Result")
	fmt.Println(result.GetNumStr())
	fmt.Println("---------------------------------")
	fmt.Println("   ResultArray Y: ", nRt.Y.String())
	fmt.Println("Result Precision: ", result.GetPrecision())
	fmt.Println("---------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)
	fmt.Println("=================================")
	if nRt.ResultAry.GetNumStr() == expected {
		fmt.Println("SUCCESS!!! Expected Result MATCHED Actual Result!")
	} else {
		fmt.Println("FAILURE!!! Expected Result DID NOT MATCH Actual Result !!!!!!")
	}
}


func DoSquareRoot() {
	// 390626 == good
	// 2685   == good 51.8169856321264600826942986293190493782400
	// 2686.5 == good 51.821810080312709972402243542717
	//        400 good result
	//     40,000 good result
	//  4,000,000 good result
	//400,000,000 good result
	numAry := common.IntAry{}
	numAry.SetIntAryWithNumStr("2685.5")
	//    1234567890123456789012345678901234567890
	expected := "51.821810080312709972402243542717"
	sqRoot := common.SquareRootOp{}
	startTime := time.Now()
	targetPrecision := 30
	sqRoot.initialize(&numAry, targetPrecision)
	sqRoot.ComputeSquareRoot()
	methodTitle := "ComputeSquareRoot()"
	endTime := time.Now()

	du := common.DurationUtility{}
	du.SetStartEndTimes(startTime, endTime)

	fmt.Println("Square Root Result:", methodTitle)
	fmt.Println("Input String:")
	fmt.Println(numAry.NumStr)
	fmt.Println()
	fmt.Println("Input Array:")
	fmt.Println(numAry.IntAry)
	fmt.Println()
	fmt.Println("Base Pairs Array")
	fmt.Println(sqRoot.BaseNumPairs)

	fmt.Println()
	fmt.Println("Precsion: ", sqRoot.ResultPrecision)
	fmt.Println()
	fmt.Println("ResultAry:")
	fmt.Println(sqRoot.ResultAry.IntAry)
	fmt.Println()
	fmt.Println("ResultAry NumStr:")
	fmt.Println(sqRoot.ResultAry.NumStr)
	fmt.Println()
	fmt.Println("ResultAry Precision:", sqRoot.ResultAry.Precision)
	fmt.Println()
	fmt.Println()
	if sqRoot.ResultAry.NumStr == expected {
		fmt.Println("Success: Result Matches Expected Result!")
	} else {
		fmt.Println("Failure: Result DOES NOT MATCH Expected Result!")
	}
	fmt.Println("        Actual Result: ", sqRoot.ResultAry.NumStr)
	fmt.Println("      Expected Result: ", expected)
	fmt.Println("---------------------------------------------")
	display, _ := du.GetHoursTime()
	fmt.Println("Elapsed Time: ", display.DisplayStr)

}
*/
